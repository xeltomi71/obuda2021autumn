﻿using Homework.ThirdParty;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Homework.UnitTests
{
    class AccountActivityServiceTests
    {
        private AccountActivityService _accountActivityService;
      

        [SetUp]
        public void SetUp()
        {
            _accountActivityService = new AccountActivityService(new FakeAccountRepository());
        }

        [Test]
        public void TestGetActivity_EmptyAccountRepository() 
        {
            //Arrange
            var account = new Account(10);

            //Act

            //Assert
            Assert.That(() => _accountActivityService.GetActivity(account.Id), Throws.TypeOf<AccountNotExistsException>());
        }

        [Test]
        public void TestGetActivity_NotEmptyAccountRepository_NoActionsPerformed()
        {
            //Arrange
            var account = new Account(10);

            var accRepo = new FakeAccountRepository();
            accRepo.Add(account);

            _accountActivityService = new AccountActivityService(accRepo);

            //Act

            //Assert
            Assert.That(_accountActivityService.GetActivity(account.Id) == ActivityLevel.None);
        }

        [Test]
        public void TestGetActivity_NotEmptyAccountRepository_LowActionsPerformed()
        {
            //Arrange
            var account = new Account(10);
            account.Activate();
            var mockActionProvider = new Mock<IAction>();

            mockActionProvider.Setup(a => a.Execute()).Returns(true);

            for (int i = 0; i < 19; i++)
            {
                account.TakeAction(mockActionProvider.Object);
            }

            var accRepo = new FakeAccountRepository();
            accRepo.Add(account);

            _accountActivityService = new AccountActivityService(accRepo);

            //Act

            //Assert
            Assert.That(_accountActivityService.GetActivity(account.Id) == ActivityLevel.Low);
        }

        [Test]
        public void TestGetActivity_NotEmptyAccountRepository_MediumActionsPerformed()
        {
            //Arrange
            var account = new Account(10);
            account.Activate();
            var mockActionProvider = new Mock<IAction>();

            mockActionProvider.Setup(a => a.Execute()).Returns(true);

            for (int i = 0; i < 39; i++)
            {
                account.TakeAction(mockActionProvider.Object);
            }

            var accRepo = new FakeAccountRepository();
            accRepo.Add(account);

            _accountActivityService = new AccountActivityService(accRepo);

            //Act

            //Assert
            Assert.That(_accountActivityService.GetActivity(account.Id) == ActivityLevel.Medium);
        }

        [Test]
        public void TestGetActivity_NotEmptyAccountRepository_HighActionsPerformed()
        {
            //Arrange
            var account = new Account(10);
            account.Activate();
            var mockActionProvider = new Mock<IAction>();

            mockActionProvider.Setup(a => a.Execute()).Returns(true);

            for (int i = 0; i < 100; i++)
            {
                account.TakeAction(mockActionProvider.Object);
            }

            var accRepo = new FakeAccountRepository();
            accRepo.Add(account);

            _accountActivityService = new AccountActivityService(accRepo);

            //Act

            //Assert
            Assert.That(_accountActivityService.GetActivity(account.Id) == ActivityLevel.High);
        }


        [Test]
        public void TestGetAmountForActivity_NotEmptyAccountRepository() 
        {

            //Arrange
            var accRepo = new FakeAccountRepository();

            for (int i = 0; i < 10; i++)
            {
                var account = new Account(i);
                account.Activate();
                var mockActionProvider = new Mock<IAction>();

                mockActionProvider.Setup(a => a.Execute()).Returns(true);

                for (int j = 0; j < 39; j++)
                {
                    account.TakeAction(mockActionProvider.Object);
                }

                accRepo.Add(account);
            }

            _accountActivityService = new AccountActivityService(accRepo);

            //Act

            //Assert
            Assert.That(_accountActivityService.GetAmountForActivity(ActivityLevel.Medium), Is.EqualTo(10));
        }


        [Test]
        public void TestGetAmountForActivity_EmptyAccountRepository()
        {

            //Arrange
      
            //Act

            //Assert
            Assert.That(_accountActivityService.GetAmountForActivity(ActivityLevel.Medium), Is.EqualTo(0));
        }




    }
}
