﻿using Homework.ThirdParty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Homework.UnitTests
{
    class FakeAccountRepository : IAccountRepository
    {

        private readonly List<Account> _accounts = new List<Account>();
        public bool Add(Account account)
        {
            _accounts.Add(account);
            return true;
        }

        public bool Exists(int accountId)
        {
            return _accounts.Any(account => account.Id == accountId);
        }

        public Account Get(int accountId)
        {
            return _accounts.FirstOrDefault(account => account.Id == accountId);
        }

        public IEnumerable<Account> GetAll()
        {
            return _accounts.ToList();
        }

        public bool Remove(int accountId)
        {
            _accounts.Remove(this.Get(accountId));
            return true;
        }
    }
}
