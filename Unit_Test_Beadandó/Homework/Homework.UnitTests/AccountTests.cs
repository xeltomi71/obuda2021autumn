﻿using Homework.ThirdParty;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Homework.UnitTests
{
    class AccountTests
    {
        private Account _account;
        private Mock<IAction> _mockActionProvider;


        [SetUp]
        public void SetUp()
        {
            _account = new Account(10);
            _mockActionProvider = new Mock<IAction>();
        }

        [Test]
        public void TestTakeAction_NonActiveAccount()
        {
            //Arrange

            //Act

            //Assert
            Assert.That(() => _account.TakeAction(_mockActionProvider.Object), Throws.TypeOf<InactiveUserException>());
        }

        [Test]
        public void TestTakeAction_ActiveAccount_SuccesfullExecute()
        {
            //Arrange
            _mockActionProvider.Setup(a => a.Execute()).Returns(true);

            //Act
            _account.Activate();
            _account.TakeAction(_mockActionProvider.Object);

            //Assert
            Assert.That(_account.ActionsSuccessfullyPerformed,Is.EqualTo(1));
        }

        [Test]
        public void TestTakeAction_ActiveAccount_NonSuccesfullExecute()
        {
            //Arrange
            _mockActionProvider.Setup(a => a.Execute()).Returns(false);

            //Act
            _account.Activate();
            _account.TakeAction(_mockActionProvider.Object);

            //Assert
            Assert.That(_account.ActionsSuccessfullyPerformed, Is.EqualTo(0));
        }



    }
}
