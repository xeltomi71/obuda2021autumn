﻿using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;
using System.Linq;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.WidgetsAtClass
{
    class ResultWidget : BasePage
    {
        public ResultWidget(IWebDriver driver) : base(driver)
        {
        }

        // TASK 4.2: Create a new timetable webelement using FindsBy annotation
        private IWebElement timeTable => Driver.FindElement(By.Id("timetable"));

        public int GetNoOfResults()
        {
            List<IWebElement> lines = timeTable.FindElements(By.TagName("tr")).ToList();
            // TASK 4.3: Count the valid "tr" tags inside timetable webelement and return the number
            return lines.Count;
        }
    }
}
