﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using WebdriverClass.PagesAtClass;
using WebdriverClass.BeadandoWidgets;

namespace WebdriverClass.BeadandoPages
{
    class _9GagCategoryPage : BasePage
    {
        public _9GagCategoryPage(IWebDriver webDriver) : base(webDriver)
        {
        }

        public Result9GagWidget GetResultWidget()
        {
            return new Result9GagWidget(Driver);
        }




    }
}
