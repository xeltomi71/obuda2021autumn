﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using WebdriverClass.PagesAtClass;
using WebdriverClass.BeadandoWidgets;
using OpenQA.Selenium.Support.UI;
using System.IO;

namespace WebdriverClass.BeadandoPages
{
    class _9GagPage : BasePage
    {
        public _9GagPage(IWebDriver webDriver) : base(webDriver)
        {
        }

        public static _9GagPage Navigate(IWebDriver webDriver)
        {
            webDriver.Url = "https://9gag.com/";

            webDriver.Manage().Window.Maximize();

            WebDriverWait wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(25)); //Wait for the cookie button

            var button = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("button[class= ' css-1k47zha'")));

            button.Click();
            

            return new _9GagPage(webDriver);
        }

        public void createScreenshoot(string filename)
        {
            var screenShot = ((ITakesScreenshot)Driver).GetScreenshot();

            string baseDirectory = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "..\\..\\..\\screenshot\\"));

            screenShot.SaveAsFile(baseDirectory + filename, ScreenshotImageFormat.Png);
        }

        public StickyNavbarWidget GetStickyNavbar()
        {

            return new StickyNavbarWidget(Driver);

        }
    }
}
