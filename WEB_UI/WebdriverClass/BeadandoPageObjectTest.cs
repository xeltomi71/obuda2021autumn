﻿using System;
using System.Linq;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Xml.Linq;
using OpenQA.Selenium.Chrome;
using WebdriverClass.BeadandoPages;
using WebdriverClass.BeadandoWidgets;

namespace WebdriverClass
{
    class BeadandoPageObjectTest : TestBase
    {

        [Test, TestCaseSource("GagData")]
        public void PageObjectGag(string category, string pageTitle)
        {

            Result9GagWidget result = _9GagPage.Navigate(Driver).GetStickyNavbar().NavigateTo(category).GetResultWidget();

            //Checking the title
            Assert.AreEqual(result.GetTitle(), pageTitle);

            //Checking visibility of post container
            Assert.AreEqual(result.GetPostContainerVisibility(), true);

            //Checking if there are elements in the post container
            Assert.Greater(result.GetVisiblePostCount(), 0);
        }

        [Test] //Is this test supposed to fail?!
        public void GagScreenshootFailTest()
        {
            _9GagPage page = _9GagPage.Navigate(Driver);

            try
            {
                Result9GagWidget result = page.GetStickyNavbar().NavigateTo("asd").GetResultWidget();
            }
            catch (Exception)
            {
                page.createScreenshoot("GagScreenshootFailTest_" + DateTime.Now.Year.ToString()+"_"+ DateTime.Now.Month.ToString() + "_"+ DateTime.Now.Day.ToString() + "_"+ DateTime.Now.Hour.ToString() + "_" + DateTime.Now.Minute.ToString() + ".png");
            }
           

           

        }

        static IEnumerable GagData()
        {
            var doc = XElement.Load(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory) + "\\9gagdata.xml");
            return
                from vars in doc.Descendants("testData")
                let category = vars.Attribute("category").Value
                let title = vars.Attribute("title").Value
                select new object[] { category, title };
        }


    }
}
