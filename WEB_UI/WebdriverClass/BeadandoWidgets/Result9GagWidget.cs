﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.BeadandoWidgets
{
    class Result9GagWidget : BasePage
    {
        public Result9GagWidget(IWebDriver webDriver) : base(webDriver)
        {
        }

        public string GetTitle()
        {
            return Driver.Title;
        }

        public bool GetPostContainerVisibility()
        {

            IWebElement postContainer = Driver.FindElement(By.Id("stream-0"));

            if (postContainer.Displayed)
                return true;

            return false;
        }

        public int GetVisiblePostCount()
        {

            IWebElement postContainer = Driver.FindElement(By.Id("stream-0"));

            List<IWebElement> posts = postContainer.FindElements(By.TagName("article")).ToList();

            return posts.Count;
        }
    }
}
