﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using WebdriverClass.PagesAtClass;
using WebdriverClass.BeadandoPages;
using System.Threading;
using OpenQA.Selenium.Support.UI;

namespace WebdriverClass.BeadandoWidgets
{
    class StickyNavbarWidget : BasePage
    {
        public StickyNavbarWidget(IWebDriver webDriver) : base(webDriver)
        {

        }

        public _9GagCategoryPage NavigateTo(string buttonLabel)
        {

            IWebElement stickyContainer = Driver.FindElement(By.CssSelector("div[class='sticky-navbar__container']"));

            string element = "a[href='" + buttonLabel + "']";

            IWebElement navLink = stickyContainer.FindElements(By.TagName("a")).ToList().Find(x => x.Text == buttonLabel);

            navLink.Click();

            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(25)); //Wait for the cookie button

            var button = wait.Until(ExpectedConditions.TitleContains(buttonLabel));

            //Thread.Sleep(500);

            return new _9GagCategoryPage(Driver);
        }


    }
}
