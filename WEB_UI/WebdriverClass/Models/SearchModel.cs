﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.WidgetsAtClass;

namespace WebdriverClass.Models
{
    class SearchModel
    {

        public string FromCity { get; set; }
        public string ToCity { get; set; }
        public string ViaCity { get; set; }
        public SearchWidget.Reductions Reduction { get; set; }
        public SearchWidget.SearchOptions SearchOption { get; set; }

        public SearchModel()
        {

        }
    }
}
